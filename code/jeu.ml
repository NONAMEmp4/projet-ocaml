(** FICHIER REPERTORIANT LES FONCTIONS PERMETTANT LE BON FONCTIONNEMENT DU JEU EN MODE MULTIJOUEUR *)

(** Les commentaires d'une ligne de code seront placés juste au dessus de cette dernière *)


(* Type d'une carte du jeu *)
type carte = { mutable valeur : int ; mutable taureau : int} ;;


(* Alias d'une main d'un joueur *)
type main = carte array ;;


(* Correspond à une rangée quelconque du plateau *)
type rangee = { mutable contenu : carte array ; mutable longueur : int ; mutable val_taureau : int } ;;


(* Affecte le nombre de taureau d'une carte en fonction des règles du jeu *)
let val_carte ( n : int ) : int = 
(*Le nombre de taureau de la carte dépend de sa valeur : 
   -> si la valeur finit par 5 (si elle est divisible par 5), alors la carte prend 2 taureau*)
  let valeur = ref 0 in 
  if n mod 5 = 0 then 
    valeur := !valeur + 1 ;
  (*-> si la valeur finit par 0 (si elle est divisible par 5 et par 10), alors la carte prend 3 taureau, soit 2 de la condition précédente et un de plus pour
     la division par 10*)
  if n mod 10 = 0 then 
    valeur := !valeur + 2 ;
  (*-> si la valeur de la carte forme un doublet comme 11,22,33... (si elle divisible par 11), alors la carte prend 5 taureau*)
  if n mod 11 = 0 then 
    valeur := !valeur + 5 ;
  (*-> si aucune des conditions précédentes ne sont vérifiées, la carte prend un taureau par défault*)
  if !valeur = 0 then 
    valeur := 1 ;
  !valeur ;;

(* Créé une pile représentant la pioche où l'on a disposé les cartes aléatoirement *)
let empiler_cartes () : carte Stack.t =
  Random.self_init () ; 
  (*On commence par créer un tableau contenant toutes les cartes du jeu*)
  let tab : carte array = Array.make 104 {valeur = 0; taureau = 0} in
  (*le jeu est composé de 104 carte de valeur allant de 1 à 104, on crée donc un tableau de taille 104 que l'on remplu de cartes en prenant en compte 
  l'indice du tableau (+1 pour parcourir les bonnes valeur) et en appelant la fonction val_carte pour obtenir le bon nombre de taureau*)
  for i = 0 to 103 do 
    tab.(i) <- {valeur = i+1 ; taureau = val_carte (i+1)} ;
  done ;
  let p = Stack.create () in 
  (* Le but de la boucle est de parcourir le tableau de manière à introduire chaque carte dans la pile mais dans un ordre aléatoire.
  Pour faire cela, on doit d'abord prendre un indice aléatoire compris entre 0 et la taille du tableau de carte. On met alors la carte associée à cet indice 
  dans la pile, puis il faut la sortir du tableau. 
  On va alors remplacer cette carte par la carte d'indice maximal du tableau, et, à l'itération de boucle suivante, on prendra un indice aléatoire compris entre 
  0 et la taille du tableau -1 pour ne pas avoir un doublon de la dernière carte*)
  for i = 0 to 102 do 
    let r = Random.int (104-i) in 
    let carte = tab.(r) in 
    Stack.push carte p ; 
    tab.(r) <- tab.(103-i) ; 
  done ; 
  Stack.push tab.(0) p ;
  p ;;

(* Représente l'ensemble des mains des joueurs *)
let piocher (nb_joueur : int) (pioche : carte Stack.t ) : main array = 
  (*on crée une matrice permettant de stocker les cartes des joueurs*)
  let pioches : main array = Array.make_matrix nb_joueur 9 {valeur = 0 ; taureau = 0} in
  (*on initialise les valeurs des cartes comme pour une partie de carte normale : pendant neuf tour de table,
  chaque joueur prend la première carte de la pioche*)
  for i = 0 to 8 do 
    for j = 0 to nb_joueur - 1 do 
      pioches.(j).(i) <- Stack.pop pioche 
    done 
  done ; 
  pioches ;;


(* Initialise le plateau *)  
let init_plateau (pioche : carte Stack.t) : rangee array =
   (*De même que pour la fonction piocher, on crée une matrice permettant de stocker les cartes des rangées, la seule différence est qu'au début du jeu, 
     chaque rangée ne contient qu'une seuule carte, toutes les autres cartes de la matrice seront donc des cartes nulles*) 
  let plateau = Array.make 4 { contenu = [||] ; longueur = 1 ; val_taureau = 0 } in
  for i = 0 to 3 do 
    let c = Stack.pop pioche in 
    let tmp = Array.make 6 {valeur = 0 ; taureau = 0} in 
    tmp.(0) <- c ;
    plateau.(i) <- {contenu = tmp ; longueur = 1 ; val_taureau = c.taureau} ; 
  done ; 
  plateau ;;


(* Range dans l'ordre décroissant la main du joueur par le principe du tri par sélection *)
let tri_main (m : main) : unit = 
  for i = 0 to 7 do 
    let tmp = ref m.(i).valeur in 
    let indice_max = ref i in 
    for j = i to 8 do 
      if !tmp < m.(j).valeur then 
      (indice_max := j ; tmp := m.(j).valeur )
    done ; 
    let carte_min = m.(!indice_max) in 
    m.(!indice_max) <- m.(i) ; 
    m.(i) <- carte_min
  done ;;


(* Afficher la main d'un joueur donné *)
let affiche_main_joueur (pioches : main array) (numero_joueur : int) : unit =

  (* On s'assure que les préconditions sont repectées :
     Cette fonction n'étant normalement jamais utilisée par un joueur,
     et si nous n'avons pas fait d'erreurs, cette condition est en fait censée être tout le temps respectée. *)
  assert (pioches != [||]) ;

  (* Initialisation des variables qui serviront la fonction *)
  let i_joueur = numero_joueur - 1 in (* Le décalage d'indice est dû à l'absence d'un joueur 0 *)
  let j = ref 1 in

  (* On s'assure que les préconditions sont respectées *)
  assert (Array.length pioches.(i_joueur) >= 8) ;

  (* On indique de quelle main il s'agit *)
  print_string "Main du joueur " ;
  print_int (numero_joueur) ;
  print_endline " :" ;

  (* Grâce aux autres fonctions (notamment select_carte),
     les cartes dont la valeur et le taureau ne sont pas 0 sont toutes 'rangées à gauche',
     c'est-à-dire que si l'on tombe sur une carte dont les valeurs sont 0 alors toutes celles d'après le seront aussi.
     Dès lors si c'est le cas pour la première carte alors la main est vide. *)
  if pioches.(i_joueur).(0) = {valeur = 0 ; taureau = 0}
  then print_endline "Votre main est vide"

  (* Sinon, on commence à afficher les cartes, en commençant par uniquement la première :
     On a choisi de les représenter par juste l'entier de la valeur d'un côté,
     puis la valeur du taureau, accompagnée de l'emoji taureau pour la deuxième.
     Cela permet de bien saisir de quoi il s'agit dans l'affichage. *)
  else begin 
    print_char '[' ;
    print_string "( " ;
    print_int (pioches.(i_joueur).(0).valeur) ;
    print_string " , " ;
    print_int (pioches.(i_joueur).(0).taureau) ;
    print_string "🐂 )" ;

    (* On procède ensuite pour le reste des cartes. *)
    (* Expliquons la condition : *)
    (* Le nombre de cartes de départ est 9 et, puisque l'on ne fait que s'en débarasser, alors on n'aura jamais plus de 9 cartes.
    Donc si l'on a fait assez de répétition pour tomber sur !j = 9 on peut s'arrêter là, on a affiché toute la main. *) 
    (* On a déjà expliqué comment les cartes sont rangées.
    Donc quand on parcourt les cartes, si l'on tombe sur une 'carte vide' on peut s'arrêter là aussi car les prochaines le seront tout autant. *) 
    while not ((!j = 9) || (pioches.(i_joueur).(!j) = {valeur = 0 ; taureau = 0})) do

      (* Sinon, on est dans le cas 'normal' où il y a une carte à afficher. On procède donc étape par étape pour l'afficher sous la forme choisie *)
      print_string " ; ( " ;
      print_int (pioches.(i_joueur).(!j).valeur) ;
      print_string " , " ;
      print_int (pioches.(i_joueur).(!j).taureau) ;
      print_string "🐂 )" ;
      j := !j + 1 ;
      
    done ;
    print_char ']' 
  end ;
  print_newline () ;;


(* Affiche le plateau de jeu *)
let affiche_plateau (plateau : rangee array) : unit =

  (* Pour satisfaire les préconditions : *)
  assert (Array.length plateau >= 4) ;

  print_endline "Le Plateau :" ;

  (* On procède rangée par rangée *)
  for i = 0 to 3 do

    (* Pour satisfaire les préconditions : *)
    assert (Array.length plateau.(i).contenu >= 5) ;

    (* D'abord la première carte de la rangée.
       (pas de conditionnelle, la rangée est toujours censée avoir une carte 'non vide' au début) *)
    print_string "[ " ;
    print_string "( " ;
    print_int (plateau.(i).contenu.(0).valeur) ;
    print_string " , " ;
    print_int (plateau.(i).contenu.(0).taureau) ;
    print_string "🐂 )" ;

    (* On affiche ensuite les 4 emplacements qui suivent dans la rangée. *)
    for j = 1 to 4 do

      (* Contrairement à l'affichage d'une main, on affiche tout de même quelque chose si la carte est 'vide',
         pour bien montrer le nombre d'emplacements manquants avant la fin, soit avant de devoir prendre des malus *)
      if (plateau.(i).contenu.(j) = {valeur = 0 ; taureau = 0}) then print_string " ; ()"

      (* Sinon, on procède à un affichage classique. *)
      else begin
        print_string " ; ( " ;
        print_int (plateau.(i).contenu.(j).valeur) ;
        print_string " , " ;
        print_int (plateau.(i).contenu.(j).taureau) ;
        print_string "🐂 ) " ;
      end ;
    done ;

    (* On a choisi d'afficher aussi ce qui suit pour symboliser la fin de la rangée,
       qui signifie que l'on risque de prendre des malus taureaux *)
    print_string " ; (⚠🐂)" ;
    print_string " ]" ;
    print_newline () ;
  done ;;


(* Vérifie s'il est possible de placer la carte dans la rangée choisie *)
let possible (card : carte) (plat : rangee array) (numero_rangee : int) : bool*int =

  (* Pour satsifaire les préconditions : *)
  assert (Array.length plat >= numero_rangee) ;
  assert (plat.(numero_rangee).contenu <> [||]) ;
  
  (* On définit l'indice de la dernière carte placée dans la rangée choisie (avec numero_rangee), longueur nous permettant de savoir où on en est dans la rangée *)
  let indice_derniere_carte = plat.(numero_rangee).longueur - 1 in
  
  (* On définit la différence entre la valeur de la carte choisie par le joueur et la valeur de la dernière carte sur la rangée choisie. *)
  let difference = card.valeur - plat.(numero_rangee).contenu.(indice_derniere_carte).valeur in
  
  (* On renvoie deux choses sous la forme d'un couple :
     -Le booléen qui nous indiquera s'il est possible de placer la carte selon les règles du jeu (la valeur doit être supérieure)
     -La valeur de cette différence qui nous aidera pour la fonction choix_placement ci-dessous. *)
  (difference > 0, difference) ;; 


let choix_placement (card : carte) (plat : rangee array) : string*int =

  (* Pour satisfaire les préconditions : *)
  assert (Array.length plat >= 4) ;

  (* On définit une différence de base : 104. Cette différence est volontairement supérieure à la différence maxiamale (104 - 1 = 103)
      Elle nous servira à savoir si on a trouvé une différence inférieure *)
  let dif = ref 104 in 
  let ind_rangee = ref 0 in

  (* Dans cette boucle : Pour chaque rangée du plateau, on utilise la fonction 'possible' ci-dessus.
      Si le booléen est 'true', alors on peut placer la carte dans la rangée d'indice i+1,
      et si la difference est inférieure alors on doit la placer à cette rangée selon les règles du jeu (on doit placer sur la valeur inférieure la plus grande).
      Si ces deux conditions sont respectées, alors dif prendra la valeur de cette différence plus petite et on conserve l'indice de la rangée où il faudra placer la carte. *)
  for i = 0 to 3 do 
    let couple = possible card plat i in 
    if (fst couple) && (!dif > snd couple) then begin 
      dif := snd couple ; ind_rangee := i end
  done ; 

  (* Ce cas s'applique si l'on a trouvé aucune différence plus petite, c'est-à-dire que le joueur ne peut placer sa carte choisie sur aucune des rangées
      Il doit alors, selon les règles, choisir une rangée dont il prendra tous les taureaux (ce dont on se charge dans une autre fonction).
      On le laissera donc choisir la rangée à l'aide d'un read_int lors du placement des cartes. La valeur retournée est un couple dont la première valeur est une chaîne,
      'j' pour joueur, permettant de savoir que ce sera au joueur de choisir la rangée, et qu'il va y avoir un remplacement d'une rangée.
      La deuxième valeur du couple n'est pas utile à ce moment car elle sera remplacée par un read_int *)
  if !dif = 104 then
    ("j",0)
    (* Si l'on est pas dans le cas précédent, alors, selon les règles, il y a forcément un endroit où le joueur sera obligé de placer sa carte.
    On n'incite alors même pas le joueur à choisir une rangée et on prend directement l'indice donné par la boucle ci-dessus, qui sera en deuxième place du couple.
    La première place indique 'o' pour ordinateur, entre guillemets, car puisque le joueur ne choisit pas alors c'est comme si un ordinateur plaçait la carte à sa place. *)
  else
    ("o",!ind_rangee) ;;

(* Permet de permutere deux cartes entre elles *)
let permute (c1 : carte) (c2 : carte) : unit =
  let tmp = c1 in
  c1.valeur <- c2.valeur ;
  c1.taureau <- c2.taureau ;
  c2.valeur <- tmp.valeur ;
  c2.taureau <- tmp.taureau ;;


(* Retire la carte sélectionnée par l'utilisateur et réorganise sa pioche *)
let rec select_carte (numero_joueur : int) (mains_joueurs : main array) : carte =
  assert(numero_joueur >= 1 && mains_joueurs <> [||]) ;
  print_string "Entrez la carte que vous voulez jouer : " ;
  print_newline();
  try 
    let a = read_int() in
    let n = Array.length mains_joueurs.(numero_joueur-1) in
    let carte_renvoyee = {valeur = a ; taureau = val_carte(a)} in
    let i = ref 0 in
    try
      (* On parcourt la main du joueur tant qu'on ne tombe pas sur la carte sélectionée *)
      while a <> mains_joueurs.(numero_joueur-1).(!i).valeur do 
        incr i;
      done;
      (* On permute toutes les cartes situées après celle que le joueur a sélectionné de sorte 
        à conserver l'ordre décroissant de la main du joueur et à placer la carte sélectionée en
        dernière position*)
      for j = !i to n - 2 do
        permute (mains_joueurs.(numero_joueur-1).(j)) (mains_joueurs.(numero_joueur-1).(j+1))
      done;
      (* La dernière carte de la main du joueur (qui est donc la carte sélectionée) est remplacée
        par une carte nulle afin de ne pas l'afficher *)
      mains_joueurs.(numero_joueur-1).(n-1) <- {valeur = 0 ; taureau = 0};
      carte_renvoyee;
    (* Si le joueur sélectionne une carte qui n'est pas dans son jeu, la boucle while provoquera une
      erreur "index out of bounds" donc on attrape l'erreur et on ré-appelle la fonction pour demander au joueur
      de sélectionner une carte valide *)
    with _ -> print_newline () ;
    print_endline "Vous n'avez pas cette carte dans votre jeu " ; print_newline() ;
    select_carte numero_joueur mains_joueurs;
(*Si l'utilisateur rentre une chaîne de caractère à la place d'un entier, on recommence la fonction en reprécisant le fonctionnement du jeu*)
  with int_of_string -> print_newline ();
    print_endline ("Veuillez entrer la valeur d'une carte présente dans votre main") ; 
    print_newline () ;
    select_carte numero_joueur mains_joueurs ;;  

(* Permet de placer une carte quand elle peut être jouée automatiquement et quand elle n'arrive pas à la sixième case de la rangée *)
let add_carte_auto (card : carte) (plateau : rangee array) (nb_rangee : int) : unit = 
  let indice = plateau.(nb_rangee).longueur in 
  plateau.(nb_rangee).contenu.(indice) <- card ; 
  (*On place la carte dans la première case libre de la rangée choisie*)
  plateau.(nb_rangee).longueur <- indice + 1 ; 
  plateau.(nb_rangee).val_taureau <- plateau.(nb_rangee).val_taureau + card.taureau ;;
  (*On modifie les donnée de la rangée en accord avec la carte qui vient d'être ajoutée (la longueur augmente de 1 et la valeur taureau totale augmente avec celle de
     la carte)*)


(* Permet de 'reset' (faire d'une rangee de 5 cartes une nouvelle rangee d'une seule carte) une rangée du plateau quand on y ajoute une sixième carte et renvoie le malus à donner au joueur*)
let reset_rangee (card : carte) (plateau : rangee array) (nb_rangee : int) : int = 
  plateau.(nb_rangee).contenu.(0) <- card ;
  for i = 1 to 4 do 
    plateau.(nb_rangee).contenu.(i) <- {valeur = 0 ; taureau = 0} ; 
  done ;
  (*On modifie la première carte de la rangée pour que celle ci soit la carte sélectionner par le joueur, puis on remplace toutes les autres cartes par des cartes
  nulles afin de ne pas les afficher *)
  plateau.(nb_rangee).longueur <- 1 ; 
  let tmp = plateau.(nb_rangee).val_taureau in 
  plateau.(nb_rangee).val_taureau <- card.taureau ;
  tmp;;
  (*On modifie les donnée de la rangée en accord avec sa nouvelle composition*)


(* Affecte le malus au joueur s'il pose la 6ème carte *)
let affecte_malus (tableau_malus : int array) (numero_joueur : int) (nb_taureaux : int) : unit =
  assert(tableau_malus <> [||] && numero_joueur >= 1 && nb_taureaux >= 1 ) ;
  (* nb_tauraux correspond au nombre renvoyé par la fonction reset_rangee *)
  tableau_malus.(numero_joueur-1) <- tableau_malus.(numero_joueur-1) + nb_taureaux;;

(* Effectue un tri à bulles pour déterminer l'ordre des cartes jouées *)
let ordre_du_tour (t : (carte*int) array) : unit =
  (* Pour satisfaire les préconditions : *)
  assert (t <> [||]) ;
  for i = 1 to Array.length t - 1 do
   for j = 0 to Array.length t - 1 - i do
      if (fst t.(j)).valeur > (fst t.(j+1)).valeur
      then let tmp = t.(j) in
        begin
          t.(j) <- t.(j+1) ;
          t.(j+1) <- tmp ;
      end ;       
    done ;
  done ;;

(*Permet à un joueur de choisir une rangée du plateau sans lever d'erreur quand la valeur rentrée par le joueur n'est pas comprise entre 1 et 4*)
let rec choix_rangee (nb_joueur : int ): int = 
  print_string "Joueur " ; 
  print_int (nb_joueur) ;
  print_endline " choisissez la rangée dans laquelle vous voulez jouer :" ;
  try 
    let rangee_selectionnee = read_int ()-1 in
    (*la conditionnelle vérifie la postcondition et rappelle la fonction choix_rangée si besoin*)
    if (rangee_selectionnee >= 0 && rangee_selectionnee <= 3) 
      then rangee_selectionnee
    else begin 
    print_newline () ; 
    print_endline ("Veuillez entrer une valeur comprise entre 1 et 4") ; 
    print_newline () ;
    choix_rangee (nb_joueur) ;
    end ; 
(*Si l'utilisateur rentre une chaîne de caractère à la place d'un entier, on recommence la fonction en reprécisant le fonctionnement 
du jeu*)
  with int_of_string -> begin 
    print_newline () ; 
    print_endline ("Veuillez entrer une valeur comprise entre 1 et 4") ; 
    print_newline () ;
    choix_rangee nb_joueur ;
  end ;;

(* Fonction permettant de représenter l'ensemble des manches qui seront jouées *)  
let manche (nb_joueur : int) (tableau_malus : int array) : unit = 
  assert(nb_joueur >= 1 && nb_joueur <= 6) ;
  (* La pioche *)
  let pioche = empiler_cartes () in
  (* Représente l'ensemble des mains des joueurs *)
  let mains_joueurs = piocher nb_joueur pioche in
  (* Servira à indiquer les actions effectuées par les joueurs *)
  let actions = Array.make 4 " " in
  let plateau = init_plateau pioche in
  (*tri des mains de tout les joueurs*)
  for i = 0 to nb_joueur - 1 do 
    tri_main mains_joueurs.(i) ;
  done ; 
  (*chaque itération de la boucle correspond au tour d'un joueur*)
  for i = 0 to 8 do
    
    (*on initialise un tableau permettant de répertorier les cartes jouées par les joueurs, que l'on triera ensuite*)
    let carte_jouees = Array.make nb_joueur ({valeur = 0; taureau = 0}, 0) in 
    for i = 1 to nb_joueur do 
      ignore (Sys.command "clear");
      affiche_main_joueur mains_joueurs i ; 
      affiche_plateau plateau ; 
      let carte_retiree = select_carte i mains_joueurs in
      carte_jouees.(i-1) <- (carte_retiree, i) ;
    done ;
    ordre_du_tour carte_jouees ; 
  (*on joue ensuite les cartes dans l'ordre, en précisant quel joueur doit faire une action quand c'est nécessaire*)
    for i = 0 to nb_joueur - 1 do
      let carte_a_placer = fst carte_jouees.(i) in 
      let test = choix_placement carte_a_placer plateau in 
    (*Si le joueur ne peut pas poser sa carte en fin de rangée, on réinitialise la rangée et on affecte le malus au joueur *)
      if fst test = "j" then begin
        ignore (Sys.command "clear");
        affiche_plateau plateau ;
        let rangee_selectionnee = choix_rangee (snd carte_jouees.(i)) in
        affecte_malus tableau_malus (snd carte_jouees.(i)) (reset_rangee carte_a_placer plateau rangee_selectionnee) ;
        
        (* On résume l'action qui vient d'arriver et on l'ajoute au tableau d'actions pour pouvoir l'afficher au début du prochain tour, avec les informations nécessaires. *)
        actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a pris les taureaux de la rangee "^(string_of_int (rangee_selectionnee+1))^" et l'a remplacée par sa carte de valeur "^(string_of_int (fst carte_jouees.(i)).valeur)^"."
      end
      else 
      (*on réinitialise la rangée si elle contient déjà 5 cartes en donnant le malus au joueur*)
      if plateau.(snd test).longueur = 5 
      then 
        begin
          affecte_malus tableau_malus (snd carte_jouees.(i)) (reset_rangee carte_a_placer plateau (snd test)) ;
          
          (* On résume l'action qui vient d'arriver et on l'ajoute au tableau d'actions pour pouvoir l'afficher au début du prochain tour, avec les informations nécessaires. *)
          actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a pris les taureaux de la rangee "^(string_of_int ((snd test) + 1))^" et l'a remplacée par sa carte de valeur "^(string_of_int carte_a_placer.valeur)^"." ;
        end
      else
        begin
          (* Si la rangée ne contient pas 5 cartes, on peut rajouter celle choisie a la fin de la rangée *) 
          add_carte_auto carte_a_placer plateau (snd (test))  ;
          
          (* On résume l'action qui vient d'arriver et on l'ajoute au tableau d'actions pour pouvoir l'afficher au début du prochain tour, avec les informations nécessaires. *)
          actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a joué la carte de valeur "^(string_of_int carte_a_placer.valeur)^" dans la rangee "^(string_of_int ((snd test) + 1))^"." ;
        end
    done ;
    (* Le tour est fini, on fait un rapide résumé de ce qui vient de se passer *)
    ignore (Sys.command "clear");
    
    (* On affiche le plateau dans sa situation actuelle *)
    affiche_plateau plateau ;
    print_newline () ;
    
    (* On donne le total de malus de chaque joueur *)
    for i = 0 to nb_joueur - 1 do 
      print_string "Le total des malus du joueur " ; 
      print_int (i+1) ; 
      print_string " vaut : " ;
      print_int tableau_malus.(i) ;
      print_newline () ;
    done ;
    (* On résume le tour précédent. *)
    print_newline () ;
    print_endline "Résumé du tour précédent :" ;

    (* On affiche toutes les actions que l'on a ajouté au tableau au fil du tour précédent. *)
    for j = 0 to nb_joueur - 1 do
      print_endline actions.(j) ;
    done ;
    print_newline () ;
                         
    (* Puisqu'il y a dans ce programme un grand nombre de 'clear' qui effacent ce qui précède,
     on marque un temps d'arrêt en demandant aux joueurs d'entrer quelque chose.
    Tant qu'il n'auront rien entré, il pourront observer la situation actuelle de la partie *)
    print_endline "Pressez entrée pour continuer." ;
    let s = ref (read_line ()) in  
      
      (* Evidemment, on souhaitait juste marquer un temps, c'est pourquoi la chaîne donnée ne nous intéresse en fait pas du tout.
      Mais si on n'en fait rien, on risque d'avoir des erreurs du type 'unused_variable' donc on la manipule juste histoire de pouvoir avancer. *)
    s := "" ;  
    ignore (Sys.command "clear");
  done ;;

let classement_final (nb_joueur : int) (tab : int array) : int array =
  (*On crée un tableau permettant de classer les joueurs*)
  let classement = Array.make nb_joueur 0 in 
  let tri a b = 
    if a < b then -1 
    else if a = b then 0
    else 1 in
    (*On crée une copie triée du tableau des malus*)
  let tableau_malus = Array.copy tab in 
  Array.sort tri tableau_malus ; 
  (*On parcourt la liste pour trouver l'indice du joueur ayant le moins de points malus*)
  for i = 0 to Array.length classement - 1 do 
    let num_joueur = ref i in 
    let min = ref (tableau_malus.(!num_joueur)) in  
    for j = 0 to nb_joueur - 1 do 
      if !min = tab.(j) then
        num_joueur := j ;
    done ; 
    classement.(i) <- !num_joueur + 1 ;
  done ; 
  classement ;;

let partie_entiere (nb_joueur : int) : unit = 
  (* Création d'un tableau permettant de stocker les taureaux accumulés par les différents joueurs 
  (le tableau aura une longeur égale au nombre de joueurs de sorte à ce que l'élément 1 du tableau 
  soit le nombre de taureaux accumulé par joueur 1 etc ...) *)
  let tableau_malus = Array.make nb_joueur 0 in 
  (*On utilise une boucle while qui s'arrête dès qu'un joueur obtient plus de 66 malus*)
  while (Array.for_all (fun x -> x<66) tableau_malus ) do 
    manche nb_joueur tableau_malus ;
  done ;
  (*Enfin, on affiche le classement dès que le jeu se termine*)
  let classement = classement_final nb_joueur tableau_malus in 
  print_newline () ;
  print_string ("Le joueur n°") ;
  print_int classement.(0) ;
  print_string (" est premier avec ") ;
  print_int tableau_malus.(0) ;
  print_endline (" points malus. Félicitation !!") ; 
  print_newline () ;
  for i = 1 to nb_joueur - 1 do 
    print_string ("Le joueur n°") ;
    print_int classement.(i) ;
    print_string (" est à la ") ; 
    print_int ( i + 1) ;
    print_string ("ème place avec ") ; 
    print_int tableau_malus.(classement.(i)-1) ;
    print_endline (" points malus") ; 
    print_newline () ;
  done ;;

