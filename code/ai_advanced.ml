(** FONCTIONNEMENT DE l'IA AVANCEE *)


(*
  -> Soit i ∈ {1;2;3;4}
  -> On appelle e_i l'écart entre le numéro de la carte de l'IA et le numéro de la carte posée en tête de la rangée i sur laquelle elle doit jouer cette carte.
  (Par exemple, si l'IA possède la carte 31 et que sur la 3ème rangée du plateau est posée en tête la carte 9 : e_3 = 31 - 9 = 22).
  -> On appelle l_i la longueur de la rangée i 
  -> On appelle "carte jouable" une carte dont le numéro est supérieur au numéro d'une des cartes posée en tête de rangée. En d'autres
  termes, une carte pour laquelle il existe i tel que e_i > 0. (Par exemple, si l'IA possède les cartes 99 et 41 et que sur le plateau 
  est posée la carte 95 alors la carte 99 est jouable car 99 > 95 mais la carte 41 n'est pas jouable car 41 < 95)
  -> On appelle "carte critique" une carte qui, avant que les autres joueurs n'aient joué leur carte, n'est pas la 6ème carte d'une rangée i
  (Par exemple, si l'IA possède la carte 6 et que la rangée i est : 1 - 2 - 3 - 4 - 5, la carte 6 est une carte critique de la rangée i.
  On dira alors que la rangée i est critique car elle est de longueur 5).



-> La première chose que l'IA doit se demander est "ai-je des cartes jouables ?"

  -> Si oui, l'IA doit se demander si elle possède des cartes jouables non critiques 
  
    -> Si oui, l'IA doit se demander si elle possède dans sa main une carte vérifiant : e_i ≤ 15 - l_i où i correspond à la rangée où la carte sélectionnée doit être jouée
      
      -> Si oui, l'IA doit donc sélectionner la carte, parmi ses cartes vérifiant l'inégalité, celle ayant le plus petit e_i de sorte à assurer son coup.
        D'où vient cette inégalité tordue : cette inégalité "garantit" que l'IA ne posera pas la 6ème carte et ne prendra donc pas de malus.
        Garantit est entre guillemets car même si l'IA possède dans son jeu une carte jouable non critique vérifiant cette inégalité, elle peut tout de même poser la 6ème carte.
        Par exemple, si l'IA possède dans son jeu les cartes 13 et 29 et sur le jeu sont posées les cartes 3 - 22 - 23 sur la rangée 2, l'IA comprend qu'elle ne peut pas 
        jouer la carte 13 car elle est non jouable mais par contre elle peut jouer sa carte 29 qui est non critique sur la rangée 2 et on a bien : 
        e_2 = 29 - 23 = 6 ≤ 15 - l_2 = 15 - 3 = 12. L'IA peut donc placer le 29 sur la rangée 2.
        Cependant, si 2 autres joueurs étaient en possession de la carte 25 et 26 (par exemple) et qu'ils décidaient de la jouer, l'IA poserait la 6ème carte.
        Néanmoins, cette situation n'est pas très fréquente  donc on se laisse une assez grande marge qui assure (en majorité) à l'IA de ne pas poser la 6ème carte.
        Si on avait voulu assurer à 100% qu'elle ne pose pas la 6ème carte, l'inégalité deviendrait e_i ≤ 5 - l_i (mais ça deviendrait bien trop restrictif).

      -> Si non, l'IA doit donc se demander si elle possède des cartes non jouables.

        -> Si oui, l'IA sélectionne alors sa plus petite carte (qui est donc non jouable car il existe au moins une carte non jouable d'après la condition imposée) et décide
        de remplacer la rangée ayant le plus petit val_taureau afin de prendre le moins de malus possible.

        -> Si non, l'IA ne peut donc pas procéder à un remplacement de rangée car elle ne possède que des cartes jouables, elle joue alors celle pour laquelle e_i est le plus
        petit afin qu'elle puisse peut-être, avec de la chance, ne pas poser la 6ème carte et donc ne pas prendre de malus

    -> Si non, l'IA doit donc se demander si elle possède des cartes non jouables.

      -> Si oui, elle se demande si, parmi ces cartes jouables, une de ces cartes vérifie e_i > 15

        -> Si oui, elle joue alors sa plus grande carte parmi les cartes vérifiant cette inégalité.
        Pourquoi cette inégalité : eh bien, l'IA espère avoir de la chance, en effet, il y a une probabilité tout de même assez forte qu'un autre joueur veuille placer 
        sa carte sur la même rangée quie l'IA mais que pour sa part, e_i < 15, ainsi, ce joueur sera ammené à jouer en premier et donc prendre la rangée et l'IA ne 
        prendra alors pas le malus, la valeur 15 a été choisie arbitrairement et semble très raisonnable.

        -> Si non, elle doit sélectionner la rangée pour laquelle val_taureau est le plus petit (i.e. celle contenant le moins de malus) et doit se demander si sa longueur vaut 5

          -> Si oui, elle se demande alors si elle possède des cartes jouables sur cette rangée

            -> Si oui, elle joue alors sa plus grande carte jouable sur cette rangée (l'IA joue sa plus grande carte de sorte à espérer qu'un autre joueur en jouera une plus petite
            et donc prendra les malus à sa place).

            -> Si non, elle sélectionne sa plus petite carte et remplace cette rangée.

          -> Si non, elle joue sa plus petite carte et remplace cette rangée

      -> Si non, elle ne possède alors que des cartes jouables et donc joue sa plus grande carte de sorte à espérer qu'un autre joueur joue une carte plus petite sur cette même rangée
      et donc prenne les malus à sa place.

  -> Si non, elle ne possède aucune carte jouable et donc joue sa plus petite carte et remplace la rangée contenant le moins de malus

*)




(* PROGRAMMATION *)



exception Break;;


(* Fonction permettant de déterminer la tête ayant la plus petite valeur parmi les 4 rangées *)
let recherche_val_tete_min (plateau : Jeu.rangee array) : int = 
  assert(plateau <> [||]);
  (* On initialise le minimum par la tête de la première rangée *)
  let mini = ref plateau.(0).contenu.((plateau.(0).longueur)-1).valeur in
  for i = 1 to 3 do
    let tete_rangee_i = ref plateau.(i).contenu.((plateau.(i).longueur)-1).valeur in
    (* Si la valeur de la tête de la rangée i est plus petite que celle initialisée par défaut, mini devient la valeur de la tête de cette dite rangée *)
    if (!tete_rangee_i < !mini) then
      mini := !tete_rangee_i
  done;
  !mini;;


(* Fonction permettant de renvoyer le tableau des cartes jouables de l'IA *)
let cartes_jouables (plateau : Jeu.rangee array) (main_ia : Jeu.main) : Jeu.main =
  assert(plateau <> [||] && main_ia <> [||]) ;
  let n = Array.length main_ia in
  let val_tete_min = recherche_val_tete_min plateau in
  (* Tableau voué à contenir les cartes jouables *)
  let tab_cartes_jouables = Array.make 9 {Jeu.valeur = 0 ; Jeu.taureau = 0} in
  try
    for i = 0 to n - 1 do
      (* carte_main représente la carte dans la main de l'IA d'indice i *)
      let carte_main = main_ia.(i) in
      (* si sa carte est "supérieure au seuil requis" alors on l'ajoute à tab_cartes_jouables *)
      if carte_main.valeur > val_tete_min then
        tab_cartes_jouables.(i) <- carte_main
      else
        (* sinon, si on tombe sur une carte ne respectant pas ce seuil, la main étant triée dans l'ordre décroissant, les suivantes 
          ne le respecte pas non plus donc on sort de la boucle et on renvoie tab_cartes_jouables *)
        raise Break;
    done;
    tab_cartes_jouables;
  with Break -> tab_cartes_jouables;;


(* Fonction permettant de savoir quelles sont les rangées critiques i.e. les rangées contenant 5 cartes ou non *)
let est_critique (plateau : Jeu.rangee array) : (bool array) =
  assert(plateau<>[||]);
  (* Si la 1ère case du tableau vaut true alors cela signifie qu'il existe au moins une rangée critique dans le plateau
    Si la 2ème case du tableau vaut true alors la première rangée est critique, si elle vaut false, la 1ère rangée n'est pas critique.
    De même pour la 3ème, 4ème et 5ème case du tableau *)
  let tab_critique = Array.make ((Array.length plateau) + 1) false in
  (* On parcourt l'entièreté du plateau *)
  for i = 0 to Array.length plateau - 1 do
    (* Si une des rangées est de longueur 5 alors la rangée est critique donc on affecte true a tab_critique et
       true à la première case de tab_critique *)
    if plateau.(i).longueur = 5 then
      begin
        tab_critique.(i+1) <- true ;
        tab_critique.(0) <- true ;
      end ;
  done;
  tab_critique;;


(* Fonction permettant de renvoyer le tableau des cartes jouables non critiques *)
let cartes_jouables_non_critiques (plateau : Jeu.rangee array) (tab_cartes_jouables : Jeu.main) : Jeu.main =
  assert(plateau <> [||] && tab_cartes_jouables <> [||]);
  (* Tableau de booléen indiquant si la rangée i est critique ou non *)
  let tab_rangees_critiques = est_critique plateau in
  (* Tableau voué à contenir les cartes jouables non critiques qui est pour le moment identique au tableau des cartes jouables *)
  let tab_cartes_jouables_non_critiques = Array.copy tab_cartes_jouables in
  let n = Array.length tab_cartes_jouables in
  (* On parcourt l'entièreté du plateau *)
  for i = 1 to Array.length plateau - 1 do
    (* On vérifie déjà si la rangée i est critique ou non *)
    if tab_rangees_critiques.(i) then
      begin
        (* il faut donc vérifier quelles sont les cartes qui sont critiques : on parcourt donc tab_cartes_jouables *)
        for j = 0 to n-1 do
          (* On fait appel à la fonction 'choix_placement' pour vérifier si c'est bien sur 
          la rangée i où la carte j sera placée  *)
          let ( _ , indice_rangee ) = Jeu.choix_placement tab_cartes_jouables.(j) plateau in
          if indice_rangee = i-1 then
            (* Si la carte j est critique alors on la remplace par une carte nulle *)
            tab_cartes_jouables_non_critiques.(j) <- {valeur = 0 ; taureau = 0};
        done;
        (* Les potentielles cartes ayant été critiques doivent être permutées à la fin du tableau afin qu'elles ne soient plus
           affichées, la seule manière est de retrier toute la main de l'IA dans l'ordre décroissant, les cartes nulles seront 
           donc placées à la fin de la main et ne seront donc pas affichées *)
        Jeu.tri_main tab_cartes_jouables_non_critiques;
      end;
  done;
  tab_cartes_jouables_non_critiques;;


(* Fonction permettant de calculer e_i *)
let calcul_e_i (card : Jeu.carte) (rangee : Jeu.rangee) : int =
  assert(rangee.contenu <> [||] && rangee.longueur >= 1);
  (* On calcule donc la différence entre le numéro de la carte de l'IA et le 
    numéro de la carte posée en tête de la rangée *)
  card.valeur - rangee.contenu.(rangee.longueur - 1).valeur;;


(* Fonction permettant de créer un tableau des cartes jouables non critiques vérifiant e_i ≤ 15 - l_i avec leur e_i associé *)
let verifie_inegalitee_1 (plateau : Jeu.rangee array) (tab_cartes_jouables_non_critiques : Jeu.main) : (Jeu.carte * int) array = 
  assert(plateau <> [||] && tab_cartes_jouables_non_critiques <> [||]);
  let n = Array.length tab_cartes_jouables_non_critiques in
  (* Tableau voué à contenir les cartes vérifiant les conditions avec leur e_i associé *)
  let tab_verifie_conditions = Array.make 9 ( {Jeu.valeur = 0 ; Jeu.taureau = 0} , 0 ) in
  let j = ref 0 in
  (* On parcourt l'entièreté du tableau des cartes jouables non critiques *)
  for i = 0 to n-1 do
    (* On fait appel à 'choix_placement' pour connaître la rangee où la carte i doit être placée et donc lui affecter son e_i correspondant *)
    let ( _ , indice_rangee ) = Jeu.choix_placement tab_cartes_jouables_non_critiques.(i) plateau in
    (* On vérifie maintenant si l'inégalité est vérifiée *)
    let min_e_i = calcul_e_i tab_cartes_jouables_non_critiques.(i) plateau.(indice_rangee) in
    if ( min_e_i <= 15 - plateau.(indice_rangee).longueur ) then
      (* Si c'est le cas, on ajoute cette carte à tab_verifie_conditions avec son min(e_i) associé *)
      begin 
        tab_verifie_conditions.(!j) <- ( tab_cartes_jouables_non_critiques.(i) , min_e_i) ;
        incr j ;
      end;
  done;
  tab_verifie_conditions;; 


(* Fonction permettant de renvoyer la carte pour laquelle e_i est le plus petit *)
let min_e_i (tab : (Jeu.carte * int) array ) : Jeu.carte =
  assert(tab <> [||]);
  (* On initialise par défaut la plus petite carte à celle qui est en première position dans le tableau *)
  let carte_e_i_mini = ref tab.(0) in
  let n = Array.length tab in
  try
    (* On parcourt le reste du tableau mais il faut bien faire attention au fait qu'il se peut que le dernier élément de tab_verifie_conditions puisse être 
      une carte nulle associée à un e_i nul ce qui ferait que carte_e_i_mini serait associée à un couple de ce genre et ce n'est pas ce que l'on veut car les cartes 
      nulles sont censées être des cartes qui n'existent plus donc dès qu'on tombe sur une carte nulle on break et on renvoie carte_e_i_mini *)
    for i = 1 to n-1 do
      if fst tab.(i) = {Jeu.valeur = 0 ; Jeu.taureau = 0} then
        raise Break
      (* Si le min(e_i) de la carte i est strictement plus petit que le min(e_i) de carte_e_i_mini alors carte_e_i_mini devient cette carte *)
      else if (snd tab.(i) <= snd !carte_e_i_mini) then
        carte_e_i_mini := tab.(i)
    done;
    fst !carte_e_i_mini;
  with Break -> fst !carte_e_i_mini;;


(* Fonction renvoyant les cartes jouables non critiques associées à leur e_i *)
let cartes_jouables_non_critiques_e_i (plateau : Jeu.rangee array) (tab_cartes_jouables_non_critiques : Jeu.main) : (Jeu.carte * int) array =
  assert(tab_cartes_jouables_non_critiques <> [||]);
  let n = Array.length tab_cartes_jouables_non_critiques in
  (* Tableau voué à contenir les cartes jouables non critiques avec leur e_i associé *)
  let tab_jouables_non_critiques_e_i = Array.make 9 ( {Jeu.valeur = 0 ; Jeu.taureau = 0} , 0 ) in
  (* On parcourt l'entièreté du tableau des cartes jouables non critiques *)
  for i = 0 to n-1 do
    (* On fait appel à 'choix_placement' pour connaître la rangee où la carte i doit être placée et donc lui affecter son e_i correspondant *)
    let ( _ , indice_rangee ) = Jeu.choix_placement tab_cartes_jouables_non_critiques.(i) plateau in
    let min_e_i = calcul_e_i tab_cartes_jouables_non_critiques.(i) plateau.(indice_rangee) in
    tab_jouables_non_critiques_e_i.(i) <- (tab_cartes_jouables_non_critiques.(i) , min_e_i)
  done;
  tab_jouables_non_critiques_e_i;;


(* Fonction permettant d'exhiber la carte ayant la plus petite valeur dans la main de l'IA *)
let recherche_carte_min (main_ia : Jeu.main) : Jeu.carte = 
  assert(main_ia <> [||]);
  (* On initialise le minimum par la première carte de la main de l'IA *)
  let mini = ref main_ia.(0) in
  let n = Array.length main_ia in
  try
    (* On parcourt le reste du tableau mais il faut bien faire attention au fait qu'il se peut que le dernier élément de tab_verifie_conditions puisse être 
      une carte nulle ce qui fait mini s'initialiserait à cette carte nulle, ce qu'on ne veut pas donc on break lorsque l'on tombe sur une carte nulle *)
    for i = 1 to n-1 do
      if (main_ia.(i) = {valeur = 0 ; taureau = 0}) then
        raise Break;
      (* La carte suivante est nécessairement plus petite car la main est triée dans l'ordre décroissant donc pas besoin de conditionnelles ici, mini
         s'initialise automatiquement à la carte suivante tant que l'on ne tombe pas sur une carte nulle *)
      mini := main_ia.(i)
    done;
    !mini;
  with Break -> !mini;;


(* Renvoie l'entier correspondant à l'indice de la rangée ayant val_taureau le plus petit *)
let rangee_taureau_min (plateau : Jeu.rangee array) : int =
  assert(plateau <> [||]);
  (* On initialise le minimum de val_taureau à la première rangée *)
  let val_taureau_min = ref plateau.(0).val_taureau in
  (* On initialise donc l'indice à 0 *)
  let i_min = ref 0 in
  for i = 1 to 3 do
    (* Dès que l'on tombe sur une rangée ayant un val_taureau plus petit : on remplace *)
     if plateau.(i).val_taureau < !val_taureau_min then
      begin
        val_taureau_min := plateau.(i).val_taureau ;
        i_min := i ;
      end
  done;
  !i_min;;


(* Fonction permettant de créer un tableau des cartes jouables vérifiant e_i > 15 *)
let verifie_inegalitee_2 (plateau : Jeu.rangee array) (tab_cartes_jouables : Jeu.main) : Jeu.main = 
  assert(plateau <> [||] && tab_cartes_jouables <> [||]);
  let n = Array.length tab_cartes_jouables in
  (* Tableau voué à contenir les cartes vérifiant l'inégalitée *)
  let tab_verifie_conditions = Array.make 9 {Jeu.valeur = 0 ; Jeu.taureau = 0} in
  let j = ref 0 in
  (* On parcourt l'entièreté du tableau des cartes jouables *)
  for i = 0 to n-1 do
    (* On fait appel à 'choix_placement' pour connaître la rangee où la carte i doit être placée *)
    let ( _ , indice_rangee ) = Jeu.choix_placement tab_cartes_jouables.(i) plateau in
    (* On vérifie maintenant si l'inégalitée est vérifiée *)
    let e_i = calcul_e_i tab_cartes_jouables.(i) plateau.(indice_rangee) in
    if ( e_i > 15 ) then
      (* Si c'est le cas, on ajoute cette carte à tab_verifie_conditions *)
      begin 
        tab_verifie_conditions.(!j) <- tab_cartes_jouables.(i);
        incr j ;
      end;
  done;
  tab_verifie_conditions;; 


(* Renvoie un tableau contenant l'ensemble des cartes pouvant être jouées sur la rangée dont l'indice a été passé en paramètre *)
let cartes_jouables_rangee (plateau : Jeu.rangee array) (tab_jouable : Jeu.main) (indice_rangee : int) : Jeu.main =
  assert(plateau <> [||] && tab_jouable <> [||] && indice_rangee >= 0 && indice_rangee <= 3);
  let n = Array.length tab_jouable in
  (* Tableau voué à contenir l'ensemble des cartes pouvant être jouées sur la rangée dont l'indice a été passé en paramètre *)
  let tab_cartes_jouables_rangee = Array.make n {Jeu.valeur = 0 ; Jeu.taureau = 0} in
  let indice = ref 0 in
  for i = 0 to n - 1 do
    (* On fait appel à la fonction 'choix placement' et on vérifie si la carte est bien jouable sur la rangée dont l'indice a été passé en paramètre *)
    let ( _ , j ) = Jeu.choix_placement tab_jouable.(i) plateau in
    (* Si l'indice trouvé correspond bien indice_rangee alors on ajoute la carte correspondante dans le tableau *)
    if indice_rangee = j then
      begin
        tab_cartes_jouables_rangee.(!indice) <- tab_jouable.(i) ;
        incr indice ;
      end
  done;
  tab_cartes_jouables_rangee;;

  
(* Retire la carte sélectionnée par l'utilisateur et réorganise sa pioche *)
let retire_carte (main_ia : Jeu.main) (carte : Jeu.carte) : unit =
  assert(main_ia <> [||]);
  let n = Array.length main_ia in
  let i = ref 0 in
  (* On parcourt la main de l'IA tant qu'on ne tombe pas sur la carte sélectionée *)
  while carte <> main_ia.(!i) do 
    incr i;
  done;
  (* On permute toutes les cartes situées après celle que l'IA a sélectionné de sorte 
    à conserver l'ordre décroissant de la main du joueur et à placer la carte sélectionée en
    dernière position *)
  for j = !i to n - 2 do
    Jeu.permute (main_ia.(j)) (main_ia.(j+1))
  done;
  (* La dernière carte de la main du joueur (qui est donc la carte sélectionée) est remplacée
    par une carte nulle afin de ne pas l'afficher *)
  main_ia.(n-1) <- {valeur = 0 ; taureau = 0};;


(* Permet d'effectuer une manche de jeu avec des IAs intelligentes *)
let manche_advanced (nb_joueur : int) (tableau_malus : int array) : unit = 
  assert(nb_joueur >= 1 && nb_joueur <= 6 && tableau_malus <> [||]); 
  (* La pioche *)
  let pioche = Jeu.empiler_cartes () in
  (* Représente l'ensemble des mains des joueurs *)
  let mains_joueurs = Jeu.piocher nb_joueur pioche in
  let plateau = Jeu.init_plateau pioche in
  let actions = Array.make 4 " " in
  (*tri des mains de tous les joueurs*)
  for i = 0 to nb_joueur - 1 do 
    Jeu.tri_main mains_joueurs.(i) ;
  done ; 
  (*chaque itération de la boucle correspond au tour d'un joueur*)
  for i = 0 to 8 do 
    (*on initialise un tableau permettant de répertorier les cartes jouées par les joueurs, que l'on triera ensuite.
      Le premier élément correspond à la carte que le joueur veut jouer
      Le second élément correspond au numéro du joueur *)
    let carte_jouees = Array.make nb_joueur ({Jeu.valeur = 0; Jeu.taureau = 0}, 0) in
    (* on initialise un tableau contenant les indices des rangées sur lesquelles les IAs voudront jouer si elles veulent procéder à un remplacement 
      le tableau est de longueur nb_joueur-1 car il ne sera pas utilisé par le joueur *)
    let rangee_jouees = Array.make (nb_joueur-1) 1 in
    ignore (Sys.command "clear");
    Jeu.affiche_main_joueur mains_joueurs 1 ; 
    Jeu.affiche_plateau plateau ; 
    (* On demande au joueur quelle carte veut-il jouer *)
    let carte_retiree = Jeu.select_carte 1 mains_joueurs in
    carte_jouees.(0) <- (carte_retiree, 1) ;
    (* On fait à présenter jouer les IAs *)
    for j = 2 to nb_joueur do 
      (* On créé son tableau de cartes jouables *)
      let tab_cartes_jouables = cartes_jouables plateau mains_joueurs.(j-1) in
      (* On créé son tableau de cartes jouables non critiques *)
      let tab_cartes_jouables_non_critiques = cartes_jouables_non_critiques plateau tab_cartes_jouables in
      (* On créé le tableau des cartes avec leur e_i associé vérifiant e_i ≤ 15 - l_i *)
      let tab_verifie_conditions = verifie_inegalitee_1 plateau tab_cartes_jouables_non_critiques in
      (* Si l'IA possède des cartes jouables i.e. si la première carte de sa main est non nulle *)
      if (tab_cartes_jouables.(0) <> {Jeu.valeur = 0 ; Jeu.taureau = 0 }) then
        begin
          (* Si elle possède des cartes jouables non critiques *)
          if (tab_cartes_jouables_non_critiques.(0) <> {Jeu.valeur = 0 ; Jeu.taureau = 0 }) then
            begin
              (* Si elle possède des cartes vérifiant e_i ≤ 15 - l_i *)
              if (fst tab_verifie_conditions.(0) <> {Jeu.valeur = 0 ; Jeu.taureau = 0}) then
                begin
                  (* On sélectionne le plus petit e_i parmi les cartes vérifiant cette inégalité *)
                  let carte_a_jouer = min_e_i tab_verifie_conditions in
                  carte_jouees.(j-1) <- ({valeur = carte_a_jouer.valeur ; taureau = carte_a_jouer.taureau},j);
                  retire_carte mains_joueurs.(j-1) carte_a_jouer;
                end
              (* Sinon, ses cartes vérifient e_i > 15 - l_i*)
              else
                begin
                  (* Si l'IA possède des cartes non jouables *)
                  if (tab_cartes_jouables <> mains_joueurs.(j-1)) then
                    begin
                      (* On détermine la rangée à remplacer i.e. celle contenant le moins de taureaux possible *)
                      let ind_rangee_a_remplacer = rangee_taureau_min plateau in
                      (* On choisit notre plus petite carte à jouer pour remplacer cette dite rangée *)
                      let carte_a_jouer = recherche_carte_min mains_joueurs.(j-1) in
                      rangee_jouees.(j-2) <- ind_rangee_a_remplacer;
                      carte_jouees.(j-1) <- ({valeur = carte_a_jouer.valeur ; taureau = carte_a_jouer.taureau},j);
                      retire_carte mains_joueurs.(j-1) carte_a_jouer;
                    end
                  (* Sinon, s'il n'existe aucune rangée remplaçable *)
                  else
                    begin
                      (* Il faut donc que l'IA joue sa carte avec le e_i le plus petit *)
                      let tab_jouables_non_critiques_e_i = cartes_jouables_non_critiques_e_i plateau tab_cartes_jouables_non_critiques in
                      let carte_a_jouer = min_e_i tab_jouables_non_critiques_e_i in
                      carte_jouees.(j-1) <- ({valeur = carte_a_jouer.valeur ; taureau = carte_a_jouer.taureau},j);
                      retire_carte mains_joueurs.(j-1) carte_a_jouer;
                    end
                end
            end
          (* Sinon, l'IA ne possède que des cartes jouables critiques *)
          else
            begin
              (* Si elle ne possède que des cartes jouables *)
              if (tab_cartes_jouables = mains_joueurs.(j-1)) then
                begin
                  (* L'IA décide de jouer sa plus grande carte en espérant qu'un autre joueur joue avant elle et prenne les malus à sa place *)
                  carte_jouees.(j-1) <- ({valeur = mains_joueurs.(j-1).(0).valeur ; taureau = mains_joueurs.(j-1).(0).taureau},j);
                  retire_carte mains_joueurs.(j-1) mains_joueurs.(j-1).(0);
                end 
              (* Sinon, elle possède au moins une carte non jouable *)
              else
                begin
                  (* Tableau contenant les cartes vérifiant e_i > 15 *)
                  let tab_verifie_condition = verifie_inegalitee_2 plateau tab_cartes_jouables in
                  (* S'il existe au moins une carte vérifiant e_i > 15 *)
                  if (tab_verifie_condition.(0) <> {Jeu.valeur = 0 ; Jeu.taureau = 0}) then
                    begin
                      (* L'IA joue alors sa plus grande carte de sorte à se donner la plus grande marge possible *)
                      carte_jouees.(j-1) <- ({valeur = tab_verifie_condition.(0).valeur ; taureau = tab_verifie_condition.(0).taureau},j);
                      retire_carte mains_joueurs.(j-1) tab_verifie_condition.(0);
                    end
                  (* Sinon, toutes les cartes jouables vérifient e_i ≤ 15 *)
                  else
                    begin
                      (* On stocke l'indice de la rangée ayant le moins de taureaux *)
                      let indice_mini = rangee_taureau_min plateau in
                      (* Si cette rangée ne contient pas 5 taureaux *)
                      if (plateau.(indice_mini).longueur <> 5) then
                        begin
                          (* On joue alors notre plus petite carte et on remplace cette rangée *)
                          let carte_a_jouer = recherche_carte_min mains_joueurs.(j-1) in
                          rangee_jouees.(j-2) <- indice_mini;
                          carte_jouees.(j-1) <- ({valeur = carte_a_jouer.valeur ; taureau = carte_a_jouer.taureau},j);
                          retire_carte mains_joueurs.(j-1) carte_a_jouer;
                        end
                      (* Sinon, la rangée contient alors 5 taureaux *)
                      else
                        begin
                          (* Tableau contenant les cartes jouables sur la rangée d'indice indice_mini *)
                          let tab_cartes_j = cartes_jouables_rangee plateau tab_cartes_jouables indice_mini in
                          (* S'il existe au moins une carte jouable sur la rangée d'indice indice_mini *)
                          if (tab_cartes_j.(0) <> {Jeu.valeur = 0 ; Jeu.taureau = 0}) then
                            begin
                              (* L'IA joue alors sa carte dont la valeur est la plus grande *)
                              carte_jouees.(j-1) <- ({valeur = tab_cartes_j.(0).valeur ; taureau = tab_cartes_j.(0).taureau},j);
                              retire_carte mains_joueurs.(j-1) tab_cartes_j.(0);
                            end
                          (* Sinon, on procède alors à un remplacement sur cette rangée *)
                          else
                            begin
                              (* On sélectionne notre plus petite carte *)
                              let carte_a_jouer = recherche_carte_min mains_joueurs.(j-1) in
                              carte_jouees.(j-1) <- ({valeur = carte_a_jouer.valeur ; taureau = carte_a_jouer.taureau},j);
                              rangee_jouees.(j-2) <- indice_mini;
                              retire_carte mains_joueurs.(j-1) carte_a_jouer;
                            end
                        end
                    end
                end
            end
        end
      (* Si l'IA ne possède aucune carte jouable *)
      else
        begin
          (* Elle est alors obligée de procéder à un remplacement *)
          (* Elle choisit donc sa plus petite carte et la place sur la rangée contenant val_taureau le plus petit *)
          let carte_a_jouer = recherche_carte_min mains_joueurs.(j-1) in
          let indice_rangee_taureau_min = rangee_taureau_min plateau in
          carte_jouees.(j-1) <- ({valeur = carte_a_jouer.valeur ; taureau = carte_a_jouer.taureau},j);
          rangee_jouees.(j-2) <- indice_rangee_taureau_min;
          retire_carte mains_joueurs.(j-1) carte_a_jouer; 
        end
    done;
    (* On trie carte_jouees *)
    Jeu.ordre_du_tour carte_jouees ;
    (* on joue ensuite les cartes dans l'ordre, en précisant quel joueur doit faire une action quand c'est nécessaire *)
    for i = 0 to nb_joueur - 1 do
      let carte_a_placer = fst carte_jouees.(i) in 
      let test = Jeu.choix_placement carte_a_placer plateau in 
      (* Si c'est le joueur qui joue le premier et qu'il ne peut pas placer sa carte en fin de rangée alors il doit sélectionner une rangée à remplacer *)
      if (fst test = "j" && snd carte_jouees.(i) = 1) then 
        begin
          ignore (Sys.command "clear");
          Jeu.affiche_plateau plateau ;
          let rangee_selectionnee = Jeu.choix_rangee (snd carte_jouees.(i)) in
          Jeu.affecte_malus tableau_malus (snd carte_jouees.(i)) (Jeu.reset_rangee carte_a_placer plateau rangee_selectionnee) ;
          actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a pris les taureaux de la rangee "^(string_of_int (rangee_selectionnee + 1))^" et l'a remplacée par sa carte de valeur "^(string_of_int (fst carte_jouees.(i)).valeur)^"."
        end
      (* Si c'est l'IA qui joue la première et qu'elle ne peut pas placer sa carte en fin de rangée alors elle doit sélectionner une rangée à remplacer *)
      else if (fst test = "j") then 
        begin 
          (* On sélectionne alors la rangée sur laquelle elle voulait jouer dont l'indice a été stocké dans rangee_jouees *)
          let indice_a_jouer = rangee_jouees.((snd carte_jouees.(i))-2) in
          Jeu.affecte_malus tableau_malus (snd carte_jouees.(i)) (Jeu.reset_rangee carte_a_placer plateau (indice_a_jouer)) ;
          actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a pris les taureaux de la rangee "^(string_of_int (indice_a_jouer + 1))^" et l'a remplacée par sa carte de valeur "^(string_of_int carte_a_placer.valeur)^"." ;
        end 
      else 
        (*on réinitialise la rangée si elle contient déjà 5 cartes en donnant le malus au joueur*)
        if plateau.(snd test).longueur = 5 then 
          begin 
            Jeu.affecte_malus tableau_malus (snd carte_jouees.(i)) (Jeu.reset_rangee carte_a_placer plateau (snd test)) ;
            print_int (Jeu.reset_rangee carte_a_placer plateau (snd test)) ;
            actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a pris les taureaux de la rangee "^(string_of_int ((snd test) + 1))^" et l'a remplacée par sa carte de valeur "^(string_of_int carte_a_placer.valeur)^"." ;
          end 
        (*si la rangée ne contient pas 5 cartes, on peut rajouter celle choisie à la fin de la rangée*)  
        else 
          begin
            Jeu.add_carte_auto carte_a_placer plateau (snd (test)) ;
            actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a joué la carte de valeur "^(string_of_int carte_a_placer.valeur)^" dans la rangee "^(string_of_int ((snd test) + 1))^"." ;
          end 
    done ;
    (*on affiche le résumé du tour qui vient de s'écouler : l'état du plateau après avoir joué toutes les cartes, les malus accumulés par les joueurs
    et le coup joué durant le tour par chaque joueur*)
    ignore (Sys.command "clear");
    Jeu.affiche_plateau plateau ;
    print_newline();
    for i = 0 to nb_joueur - 1 do 
      print_string "Le total des malus du joueur " ; 
      print_int (i+1) ; 
      print_string " vaut : " ;
      print_int tableau_malus.(i) ;
      print_newline () ;
    done ;
    print_newline () ;
    print_endline "Résumé du tour précédent :" ;
    for j = 0 to nb_joueur - 1 do
      print_endline actions.(j) ;
    done ;
    print_newline () ;
    print_endline "Pressez entrée pour continuer." ;
    let s = ref (read_line ()) in  
    s := "" ;  
    ignore (Sys.command "clear");
  done ;;

(*Permet à un nombre choisi de joueur de jouer au jeu avec des ia naïves*)
let partie_entiere_advanced (nb_joueur : int) : unit = 
  (* Création d'un tableau permettant de stocker les taureaux accumulés par les différents joueurs 
  (le tableau aura une longeur égale au nombre de joueurs de sorte à ce que l'élément 1 du tableau 
  soit le nombre de taureaux accumulé par joueur 1 etc ...) *)
  let tableau_malus = Array.make nb_joueur 0 in 
  (*On utilise une boucle while qui s'arrête dès qu'un joueur obtient plus de 66 malus*)
  while (Array.for_all (fun x -> x < 66) tableau_malus ) do 
    manche_advanced nb_joueur tableau_malus ;
  done ;
  (*Enfin, on affiche le classement dès que le jeu se termine*)
  let classement = Jeu.classement_final nb_joueur tableau_malus in 
  print_newline () ;
  print_string ("Le joueur n°") ;
  print_int classement.(0) ;
  print_string (" est premier avec ") ;
  print_int tableau_malus.(0) ;
  print_endline (" points malus. Félicitation !!") ; 
  print_newline () ;
  for i = 1 to nb_joueur - 1 do 
    print_string ("Le joueur n°") ;
    print_int classement.(i) ;
    print_string (" est à la ") ; 
    print_int ( i + 1) ;
    print_string ("ème place avec ") ; 
    print_int tableau_malus.(i) ;
    print_endline (" points malus") ; 
    print_newline () ;
  done ;
  let s = read_line () in  
  print_string s ;
  ignore (Sys.command "clear");
  print_newline () ;;