(* Ce fichier contient les fonctions qui serviront au jeu avec des IAs qui jouent au hasard.
   Le principe est simple : à part pour un seul joueur, à chaque fois que l'on demandait à un joueur ce qu'il voulait faire,
   on va cette fois-ci jouer une carte au hasard, ou placer dans une rangée au hasard.
   On se dit déjà que ce ne sera surement pas la meilleure méthode pour gagner *)


(* Voici donc la première fonction, elle remplacera select_carte dans le cas des joueurs qui seront remplacés par des IA. *)


(* Retire la carte sélectionnée aléatoirement par l'ordinateur et réorganise sa pioche *)
let select_carte_hasard (numero_joueur : int) (mains_joueurs : Jeu.main array) (taille : int) : Jeu.carte =

  (* Pour satisfaire les préconditions : *)
  assert (numero_joueur >= 1 && taille >= 0 && taille <= 9) ;
  
  (* Au lieu de demander à un joueur d'entrer la carte selectionnée, on prend cette fois-ci une carte aléatoire du jeu.
     Pour cela on prend un entier aléatoire entre 0 et la taille de la main et on prend la carte qui est au rang de l'entier. *)
  let entier_alea = Random.int (taille) in
  let a = mains_joueurs.(numero_joueur-1).(entier_alea).valeur in
  
  (* Pour le reste, c'est exactement la même chose que dans la première version multijoueur. *)
  let n = Array.length mains_joueurs.(numero_joueur-1) in
  let tmp = {Jeu.valeur = a ; Jeu.taureau = Jeu.val_carte(a)} in
  let i = ref 0 in
  (* On parcourt la main du joueur tant qu'on ne tombe pas sur la carte sélectionée *)
  while a <> mains_joueurs.(numero_joueur-1).(!i).valeur do 
    incr i;
  done ;
    (* On permute toutes les cartes situées après celle sélectionnée aléatoirement de sorte 
      à conserver l'ordre décroissant de la main du joueur et à placer la carte sélectionée en
      dernière position *)
  for j = !i to n - 2 do
    Jeu.permute (mains_joueurs.(numero_joueur-1).(j)) (mains_joueurs.(numero_joueur-1).(j+1))
  done;
    (* La dernière carte de la main du joueur (qui est donc la carte sélectionée) est remplacée
      par une carte nulle afin de ne pas l'afficher *)
  mains_joueurs.(numero_joueur-1).(n-1) <- {Jeu.valeur = 0 ; Jeu.taureau = 0};
  tmp;;



(* Deuxième fonction : manche. On doit faire des modifications quand au tour des joueurs. *)

(*Permet d'effectuer une manche du jeu avec des ia naïves*)
let manche_hasard (nb_joueur : int) (tableau_malus : int array) : unit = 
  (* La pioche *)
  let pioche = Jeu.empiler_cartes () in
  (* Représente l'ensemble des mains des joueurs *)
  let mains_joueurs = Jeu.piocher nb_joueur pioche in
  let plateau = Jeu.init_plateau pioche in
  let actions = Array.make 4 " " in
  (*tri des mains de tout les joueurs*)
  for i = 0 to nb_joueur - 1 do 
    Jeu.tri_main mains_joueurs.(i) ;
  done ; 
  (*chaque itération de la boucle correspond au tour d'un joueur*)
  for i = 0 to 8 do 
    (*on initialise un tableau permettant de répertorier les cartes jouées par les joueurs, que l'on triera ensuite*)
    let carte_jouees = Array.make nb_joueur ({Jeu.valeur = 0; Jeu.taureau = 0}, 0) in
    ignore (Sys.command "clear");
    Jeu.affiche_main_joueur mains_joueurs 1 ; 
    Jeu.affiche_plateau plateau ; 
    let carte_retiree = Jeu.select_carte 1 mains_joueurs in
    carte_jouees.(0) <- (carte_retiree, 1) ;
    for j = 2 to nb_joueur do 
      let carte_retiree = select_carte_hasard j mains_joueurs (9-i) in
      carte_jouees.(j-1) <- (carte_retiree, j) ;
    done ;
    Jeu.ordre_du_tour carte_jouees ; 
  (*on joue ensuite les cartes dans l'ordre, en précisant quel joueur doit faire une action quand c'est nécessaire *)
    for i = 0 to nb_joueur - 1 do
      let carte_a_placer = fst carte_jouees.(i) in 
      let test = Jeu.choix_placement carte_a_placer plateau in 
    (*Si le joueur ne peut pas poser sa carte en fin de rangée, on réinitialise la rangée et on affecte le malus au joueur *)
      if (fst test = "j" && snd carte_jouees.(i) = 1) then begin
        ignore (Sys.command "clear");
        Jeu.affiche_plateau plateau ;
        let rangee_selectionnee = Jeu.choix_rangee (snd carte_jouees.(i)) in
        Jeu.affecte_malus tableau_malus (snd carte_jouees.(i)) (Jeu.reset_rangee carte_a_placer plateau rangee_selectionnee) ;
        actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a pris les taureaux de la rangee "^(string_of_int (rangee_selectionnee + 1))^" et l'a remplacée par sa carte de valeur "^(string_of_int (fst carte_jouees.(i)).valeur)^"."
      end
      else if (fst test = "j") then begin 
        let alea = Random.int 4 in
        Jeu.affecte_malus tableau_malus (snd carte_jouees.(i)) (Jeu.reset_rangee carte_a_placer plateau (alea)) ;
        actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a pris les taureaux de la rangee "^(string_of_int (alea + 1))^" et l'a remplacée par sa carte de valeur "^(string_of_int carte_a_placer.valeur)^"." ;
      end 
      else 
      (*on réinitialise la rangée si elle contient déjà 5 cartes en donnant le malus au joueur*)
      if plateau.(snd test).longueur = 5 
      then begin 
        Jeu.affecte_malus tableau_malus (snd carte_jouees.(i)) (Jeu.reset_rangee carte_a_placer plateau (snd test)) ;
        print_int (Jeu.reset_rangee carte_a_placer plateau (snd test)) ;
        actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a pris les taureaux de la rangee "^(string_of_int ((snd test) + 1))^" et l'a remplacée par sa carte de valeur "^(string_of_int carte_a_placer.valeur)^"." ;
    end  
    else begin
        (*si la rangée ne contient pas 5 cartes, on peut rajoutée celle choisie a la fin de la rangée*) 
        Jeu.add_carte_auto carte_a_placer plateau (snd (test)) ;
        actions.(i) <- "Le joueur "^(string_of_int (snd carte_jouees.(i)))^" a joué la carte de valeur "^(string_of_int carte_a_placer.valeur)^" dans la rangee "^(string_of_int ((snd test) + 1))^"." ;
    end 
    done ;
    (*on affiche le résumé du tour qui vient de s'écouler : l'état du plateau après avoir joué toutes les cartes, les malus accumulés par les joueurs
    et le coup joué durant le tour par chaque joueur*)
    ignore (Sys.command "clear");
    Jeu.affiche_plateau plateau ;
    print_newline () ;
    for i = 0 to nb_joueur - 1 do 
      print_string "Le total des malus du joueur " ; 
      print_int (i+1) ; 
      print_string " vaut : " ;
      print_int tableau_malus.(i) ;
      print_newline () ;
    done ;
    print_newline () ;
    print_endline "Résumé du tour précédent :" ;
      for j = 0 to nb_joueur - 1 do
        print_endline actions.(j) ;
      done ;
    print_newline () ;
    print_endline "Pressez entrée pour continuer." ;
    let s = ref (read_line ()) in  
    s := "" ;  
    ignore (Sys.command "clear");
done ;;

(*Permet à un nombre choisi de joueur de jouer au jeu avec des ia naïves*)
let partie_entiere_hasard (nb_joueur : int) : unit = 
  (* Création d'un tableau permettant de stocker les taureaux accumulés par les différents joueurs 
  (le tableau aura une longeur égale au nombre de joueurs de sorte à ce que l'élément 1 du tableau 
  soit le nombre de taureaux accumulé par joueur 1 etc ...) *)
  let tableau_malus = Array.make nb_joueur 0 in 
  (*On utilise une boucle while qui s'arrête dès qu'un joueur obtient plus de 66 malus*)
  while (Array.for_all (fun x -> x < 66) tableau_malus ) do 
    manche_hasard nb_joueur tableau_malus ;
    print_newline () ;
  done ;
  ignore (Sys.command "clear") ; 
  (*Enfin, on affiche le classement dès que le jeu se termine*)
  let classement = Jeu.classement_final nb_joueur tableau_malus in 
  print_newline () ;
  print_string ("Le joueur n°") ;
  print_int classement.(0) ;
  print_string (" est premier avec ") ;
  print_int tableau_malus.(0) ;
  print_endline (" points malus. Félicitation !!") ; 
  print_newline () ;
  for i = 1 to nb_joueur - 1 do 
    print_string ("Le joueur n°") ;
    print_int classement.(i) ;
    print_string (" est à la ") ; 
    print_int ( i + 1) ;
    print_string ("ème place avec ") ; 
    print_int tableau_malus.(i) ;
    print_endline (" points malus") ; 
    print_newline ();
  done ;
  let s = read_line () in  
  print_string s ;
  ignore (Sys.command "clear");
  print_newline () ;

