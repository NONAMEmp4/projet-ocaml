open Jeu

(** fonction select_carte_hasard : Retire la carte sélectionnée aléatoirement par l'ordinateur et réorganise sa pioche
        - entrée : deux entiers, une main
        - précondition : le numéro du joueur est supérieur ou égal à 1 et la taille est comprise entre 0 et 8 inclus
        - sortie : carte
        - postcondition : la carte renvoyée par la fonction à été enlevée de la main sans perturber l'odre drécroissant
        - lève une Assert_failure si les préconditions ne sont pas respectées.
*)
val select_carte_hasard : int -> Jeu.main array -> int -> Jeu.carte

(** fonction manche_hasard : Permet d'effectuer une manche du jeu avec des ia naïves
        - entrée : un entier, un tableau d'entier
        - précondition : le numéro du joueur est supérieur à 1 et inférieur à 6 
        - sortie : rien (unit)
        - postcondition : aucune
*)
val manche_hasard : int -> int array -> unit

(** fonction partie_entiere_hasard : Permet à un nombre choisi de joueur de jouer au jeu avec des ia naïves
        - entrée : un entier
        - précondition : le numéro du joueur est supérieur à 1 et inférieur à 6 
        - sortie : rien (unit)
        - postcondition : aucune
*)

val partie_entiere_hasard : int -> unit 
