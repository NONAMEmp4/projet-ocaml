let main () =
  if (Array.length Sys.argv) <> 3 then
    print_endline "Nombre d'argument incorrect, on attend : ./jeu [mode] [entier]."
  else
    if (Sys.argv.(1) <> "multijoueur" && Sys.argv.(1) <> "solo") then
      print_endline "Ce mode n'est pas pris en compte, on attend 'multijoueur' ou 'solo'."
    (* Cas où le joueur veut jouer en mode multijoueur *)
    else if (Sys.argv.(1) = "multijoueur") then
      if (int_of_string(Sys.argv.(2)) < 1 || int_of_string(Sys.argv.(2)) > 6) then
        print_endline "Nombre de joeurs incorrect, on attend un nombre entre 1 et 6"
      else
        Jeu.partie_entiere (int_of_string(Sys.argv.(2)))
    (* Cas où le joueur veut jouer en mode solo i.e. avec des IAs*)
    else 
      (* Cas où l'on veut jouer avec des IAs naïves *)
      if (Sys.argv.(2) = "1") then
        Ai_gullible.partie_entiere_hasard 4
      (* Cas où l'on veut jouer avec des IAs avancées *)
      else if (Sys.argv.(2) = "2") then
        Ai_advanced.partie_entiere_advanced 4
      else
        print_endline "Cette difficulté n'est pas prise en compte, veuillez sélectionner 1 ou 2.";;


   
 let _ = main ()
