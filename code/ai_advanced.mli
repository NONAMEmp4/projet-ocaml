open Jeu

(** fonction rechercher_val_tete_min 
        - entrée : rangee array (le plateau)
        - précondition : le plateau est non vide
        - sortie : int
        - postcondition : renvoie la tête ayant la plus petite valeur parmi les 4 rangées
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val recherche_val_tete_min : Jeu.rangee array -> int

(** fonction cartes_jouables
        - entrée : une rangee array (le plateau) et une main (la mains de l'IA)
        - précondition : le plateau est non vide et la main de l'IA est non vide
        - sortie : carte array
        - postcondition : renvoie le tableau des cartes jouables
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val cartes_jouables : Jeu.rangee array -> Jeu.main -> Jeu.main

(** fonction est_critique
        - entrée : Une rangee array (le plateau)
        - précondition : le plateau est non vide
        - sortie : bool array
        - postcondition : Si la 1ère case du tableau vaut true alors cela signifie qu'il existe au moins une rangée critique dans le plateau
        la 2ème case du tableau vaut true alors la première rangée est critique, si elle vaut false, la 1ère rangée n'est pas critique.
        De même pour la 3ème, 4ème et 5ème case du tableau
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val est_critique : Jeu.rangee array -> bool array

(** fonction cartes_jouables_non_critiques
        - entrée : Une rangee array (le plateau) et une main (le tableau des cartes jouables) 
        - précondition : le plateau est non vide et le tableau des cartes jouables est non vide 
        - sortie : une main
        - postcondition : Renvoie le tableau des cartes jouables non critiques
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val cartes_jouables_non_critiques : Jeu.rangee array -> Jeu.main -> Jeu.main

(** fonction calcul_e_i
        - entrée : Une carte et une rangee
        - précondition : rangee.contenu est non nul et rangee.longueur est supérieure à 1
        - sortie : int
        - postcondition : Renvoie la valeur de e_i correspondante
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val calcul_e_i : Jeu.carte -> Jeu.rangee -> int

(** fonction verifie_inegalitee_1
        - entrée : Une rangee array (le plateau) et une main (le tableau des cartes jouables non critiques)
        - précondition : le plateau et le tableau des cartes jouables non critiques sont non vides
        - sortie : (carte * int) array
        - postcondition : Renvoie un tableau des cartes jouables non critiques vérifiant e_i ≤ 15 - l_i avec leur e_i associé
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val verifie_inegalitee_1 : Jeu.rangee array -> Jeu.main -> (Jeu.carte * int) array

(** fonction min_e_i
        - entrée : Un (carte*int) array 
        - précondition : Le tableau passé en paramètre est non vide
        - sortie : carte
        - postcondition : renvoie la carte pour laquelle e_i est le plus petit
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val min_e_i : (Jeu.carte * int) array -> Jeu.carte

(** fonction cartes_jouables_non_critiques_e_i
        - entrée : Une rangee array (le plateau) et une main (tableau des cartes jouables non critiques)
        - précondition : Le plateau et la main sont non vides
        - sortie : (carte * int) array
        - postcondition : renvoie un tableau des cartes jouables non critiques associées à leur e_i
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val cartes_jouables_non_critiques_e_i : Jeu.rangee array -> Jeu.main -> (Jeu.carte * int) array

(** fonction recherche_carte_min
        - entrée : Une main (la main de l'IA)
        - précondition : La main est non vide
        - sortie : Une carte
        - postcondition : exhibe la carte ayant la plus petite valeur dans la main de l'IA
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val recherche_carte_min : Jeu.main -> Jeu.carte

(** fonction rangee_taureau_min
        - entrée : Une rangee array (le plateau) 
        - précondition : Le plateau est non vide
        - sortie : int
        - postcondition : Renvoie l'entier correspondant à l'indice de la rangée ayant val_taureau le plus petit
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val rangee_taureau_min : Jeu.rangee array -> int

(** fonction verifie_inegalite_2
        - entrée : Une rangee array (le plateau) et une main (le tableau des cartes jouables)
        - précondition : Le plateau et la main sont non vides
        - sortie : int
        - postcondition : Renvoie l'entier correspondant à l'indice de la rangée ayant val_taureau le plus petit
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val verifie_inegalitee_2 : Jeu.rangee array -> Jeu.main -> Jeu.main

(** fonction cartes_jouables_rangee
        - entrée : Une rangee array (le plateau), une main (un tableau de cartes jouables) et un entier (correspondant à l'indice de la rangée)
        - précondition : Le plateau et la main sont non vides et l'entier est compris entre 0 et 3
        - sortie : int
        - postcondition : Renvoie un tableau contenant l'ensemble des cartes pouvant être jouées sur la rangée dont l'indice a été passé en paramètre
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val cartes_jouables_rangee : Jeu.rangee array -> Jeu.main -> int -> Jeu.main

(** fonction retire_carte
        - entrée : Une main (la main de l'IA) et une carte
        - précondition : La main est non vide
        - sortie : rien (unit)
        - postcondition : Retire la carte sélectionnée par l'utilisateur et réorganise sa main
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val retire_carte : Jeu.main -> Jeu.carte -> unit

(** fonction manche_advanced
        - entrée : Un entier (le nombre de joueurs) et un tableau (le tableau des malus)
        - précondition : Le nombre de joueurs est compris entre 1 et 6 et le tableau des malus est non vide
        - sortie : rien (unit)
        - postcondition : Effectue une manche de jeu avec des IAs intelligentes
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val manche_advanced : int -> int array -> unit

(** fonction partie_entiere_advanced
        - entrée : Un entier (le nombre de joueurs )
        - précondition : Le nombre de joueurs est compris entre 1 et 6
        - sortie : rien (unit)
        - postcondition : Permet à un nombre choisi de joueur de jouer au jeu avec des IAs intelligentes
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val partie_entiere_advanced : int -> unit