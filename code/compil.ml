let compil () : unit = 
  ignore (Sys.command "ocamlc -c jeu.mli") ;
  ignore (Sys.command "ocamlc -c jeu.ml") ;
  ignore (Sys.command "ocamlc -c ai_gullible.mli") ;
  ignore (Sys.command "ocamlc -c ai_gullible.ml") ;
  ignore (Sys.command "ocamlc -c ai_advanced.mli") ;
  ignore (Sys.command "ocamlc -c ai_advanced.ml") ;
  ignore (Sys.command "ocamlc -c main.ml") ;
  ignore (Sys.command "ocamlc -o jeu jeu.cmo ai_gullible.cmo ai_advanced.cmo main.cmo") ;;

let _ = compil ()