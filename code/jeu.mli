(** type carte
        - représente une carte, c'est un type produit avec deux éléments : Un entier 'valeur' et un entier 'taureau'
        - Non polymorphe (il s'agit focrément d'entiers)
        - Homogène (il s'agit focrément de deux entiers)
        - La taille n'est pas mutable mais les deux valeurs le sont.
*)
type carte = { mutable valeur : int ; mutable taureau : int}

(** type main
        - représente la main d'un joueur, alias pour un tableau de cartes.
        (- Homogène, non polymorphe, non mutable mais les cartes et leurs valeurs le sont)
*)
type main = carte array

(** type rangee
        - représente une rangée du plateau de jeu, c'est un type produit avec trois éléments :
            * Un tableau de cartes (il a été décidé de ne pas les appeler des mains car ce n'en sont pas réelement) 'contenu', les cartes qui sont ajoutées à la rangée.
            * Un entier 'longueur', le nombre de cartes qui y sont placées.
            * Un entier 'val_taureau', le total des taureaux des cartes qu'elle contient.
        (- Non homogène, non polymorphe)
        - Le contenu, la longueur et la val_taureau sont tous les 3 mutables.
*)
type rangee = { mutable contenu : carte array ; mutable longueur : int ; mutable val_taureau : int }

(** fonction val_carte : associe à l'entier donné la valeur en taureaux de la carte dont le numéro est l'entier.
        - entrée : un entier
        - précondition : aucune
        - sortie : un entier, la valeur en taureaux de la carte associée à l'entier
        - postcondition : aucun effet de bord
*)
val val_carte : int -> int

(** fonction empiler_cartes : ajoute toutes les cartes d'un tableau dans une pile
        - entrée : un tableau de cartes
        - précondition : le tableau est de taille 104
        - sortie : une pile de cartes
        - postcondition : la grande majorité des cartes du tableau ont changé de rang
*)
val empiler_cartes : unit -> carte Stack.t

(** fonction piocher : fait piocher un certain nombre de joueur
        - entrée : un entier et une pile de cartes
        - précondition : la pile contient assez de cartes pour chaque joueur (9 fois le nombre de joueur ou plus)
        - sortie : un tableau de main
        - postcondition : toutes les mains contiennent 9 cartes
*)

val piocher : int -> carte Stack.t -> main array

(** fonction init_plateau : initialise un plateau de jeu
        - entrée : une pile de cartes
        - précondition : la pile de carte contient au moins 4 cartes
        - sortie : un tableau de rangee
        - postcondition : chaque rangée contitent 1 carte et 8 cartes nulles
*)

val init_plateau : carte Stack.t -> rangee array

(** fonction tri_main : initialise un plateau de jeu
        - entrée : une main
        - précondition : aucune
        - sortie : aucune
        - postcondition : les cartes de la main sont rangées dans l'ordre décroissant
*)

val tri_main : main -> unit

(** fonction affiche_main_joueur : affiche l'une des mains d'un tableau de mains
        - entrées : une tableau de mains et un entier
        - précondition : Le tableau est non vide et la mains choisie de taille >= 8
        - sortie : rien (unit)
        - postcondition : Toute la main, sous une forme simplifiée, a été affichée.
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val affiche_main_joueur : main array -> int -> unit

(** fonction affiche_plateau : affiche le plateau
        - entrée : une tableau de rangées (le plateau)
        - précondition : le tableau est de taille 4 ou plus, le contenu de chaque rangée est de taille 5 ou plus
        - sortie : rien (unit)
        - postcondition : Le plateau a été affiché sous une forme simplifiée
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val affiche_plateau : rangee array -> unit 

(** fonction possible : indique si l'on peut placer une carte à une rangée du plateau
        - entrées : une carte, un tableau de rangées (le plateau) et un entier
        - précondition : Si n est l'entier entré, le tableau a au moins n rangées, la rangée n'est pas nulle
        - sortie : un couple -> un booléen (indique si la carte est plaçable) et un entier (la différence entre la valeur de la carte et la valeur de la dernière carte de la rangée donnée)
        - postcondition : aucune
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val possible : carte -> rangee array -> int -> (bool*int)

(** fonction choix_placement : indique si le joueur devra choisir la rangée où mettre sa carte ou si le choix n'est pas nécessaire.
        - entrées : une carte et un tableau de rangées (le plateau)
        - précondition : le tableau est de taille 4 ou plus
        - sortie : un couple -> une chaîne de caractère ("j" ou "o" selon le cas où le joueur devra choisir sa rangée ou non) et un entier (l'indice de la rangée où il faudra placer la carte dans le cas "o", et 0 sinon)
        - postcondition : aucune
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val choix_placement : carte -> rangee array -> (string*int)

(** fonction permute : Permute deux cartes entre-elles
        - entrée : deux cartes
        - précondition : aucune
        - sortie : rien (unit)
        - postcondition : La carte 1 est devenue la carte 2 et la carte 2 est devenue la carte 1
*)
val permute : carte -> carte -> unit

(** fonction select_carte : Retire la carte sélectionnée par l'utilisateur et réorganise sa pioche
        - entrée : un entier correspondant au numéro du joueur et un tableau correspondant à l'ensemble des mains des joueurs
        - précondition : le numéro du joueur est supérieur à 1 et le tableau des mains des joueurs est non vide
        - sortie : carte
        - postcondition : La main du joueur auquel on a retiré une carte est toujours trié dans l'ordre décroissant et sa dernière carte est une carte nulle de sorte à ne pas l'afficher au tour suivant
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val select_carte : int -> main array -> carte

(** fonction add_carte_auto : Place une carte quand elle peut être jouée automatiquement et quand elle n'arrive pas à la sixième case de la ligne
        - entrée : une carte, un tableau de rangee et un entier
        - précondition : l'entier est compris entre 0 et 3 et la rangée dont l'entier est l'indice est de longueur strictement iférieure à 5
        - sortie : rien (unit)
        - postcondition : la carte est placée à la fin de la rangée, la longueur de la rangée à augmentée de 1 et sa valeur taureau à augmentée selon le 
        nombre de taureau de la carte
*)

val add_carte_auto : carte -> rangee array -> int -> unit


(** fonction reset_rangee : Permet de réinitialiser une rangée du plateau quand on y ajoute une sixième carte et renvoie le malus à donner au joueur
        - entrée : une carte, un tableau de rangee et un entier
        - précondition : l'entier est compris entre 0 et 3 et la rangée dont l'entier est l'indice est de longueur égale à 1
        - sortie : un entier
        - postcondition : la rangée modifiée contient la carte placée en paramètre et 5 cartes nulles, l'entier renvoyé correspond à la valeur taureau de la rangée, 
        et la longueur et la valeur taureau de la rangée ont été réinitialisés
*)

val reset_rangee : carte -> rangee array -> int -> int 

(** fonction affecte_malus : Affecte le malus au joueur s'il pose la 6ème carte
        - entrée : le tableau des malus, un entier correspondant au numéro du joueur et un entier correspondant à un nombre de taureaux
        - précondition : le numéro du joueur est supérieur à 1, le tableau des malus est non vide et le nombre de taureaux est supérieur à 5
        - sortie : rien (unit)
        - postcondition : Le malus affecté au joueur passé en paramètre a bien été enregistré dans la bonne case du tableau
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val affecte_malus : int array -> int -> int -> unit 

(** fonction ordre du tour : trie une tableau de couple (carte et entier) dans l'ordre des valeurs des cartes.
        - entrée : un tableau de couples (carte*int)
        - précondition : le tableau n'est pas vide
        - sortie : rien (unit)
        - postcondition : les couples du tableaux sont triés selon l'ordre des valeurs des cartes.
        - lève une Assert_failure si les préconditions ne sont pas respectées
*)
val ordre_du_tour : (carte*int) array -> unit

(** fonction choix_rangee : Permet à un joueur de choisir une rangée du plateau sans lever d'erreur quand la valeur rentrée par le joueur n'est pas comprise entre 1 et 4
        - entrée : un entier
        - précondition :le numéro du joueur est inférieur au nombre du joueur ayant lancé la partie
        - sortie : un entier 
        - postcondition : l'entier renvoyé est l'entier rentré par le joueur - 1 et est compris entre 0 et 3
*)

val choix_rangee : int -> int 

(** fonction manche : Permet d'effectuer une manche du jeu
        - entrée : un entier, un tableau d'entier
        - précondition : le numéro du joueur est supérieur à 1 et inférieur à 6 
        - sortie : rien (unit)
        - postcondition : aucune
*)

val manche : int ->int array -> unit

(** fonction classement_final : Permet d'établir le classement des joueurs à la fin de la partie
        - entrée : un entier, un tableau d'entier
        - précondition : le nombre de joueur est égale à la longueur du tableau
        - sortie : un tableau d'entier
        - postcondition : le tableau des malus passé en paramètre reste inchangé
*)

val classement_final : int -> int array -> int array

(** fonction partie_entiere : Permet à un nombre choisi de joueur de jouer au jeu sans ia 
        - entrée : un entier
        - précondition : le numéro du joueur est supérieur à 1 et inférieur à 6 
        - sortie : rien (unit)
        - postcondition : aucune
*)

val partie_entiere : int -> unit 
